package com.example.logisctest.presenters;

import com.example.logisctest.interactor.MainInteractorImpl;
import com.example.logisctest.interfaces.MainInteractor;
import com.example.logisctest.interfaces.MainPresenter;
import com.example.logisctest.interfaces.MainUi;

public class MainPresenterImpl implements MainPresenter {

    private final MainUi ui;
    private final MainInteractor interactor;

    public MainPresenterImpl(MainUi ui) {
        this.ui = ui;
        this.interactor = new MainInteractorImpl(this);
    }

    @Override
    public void directions(int N, int M) {

    }

    @Override
    public void showResult(String result) {

    }
}
