package com.example.logisctest.interfaces;

public interface MainPresenter {
    void directions(int N, int M);
    void showResult(String result);
}
