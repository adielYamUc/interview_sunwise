package com.example.logisctest.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.logisctest.R;
import com.example.logisctest.interfaces.MainPresenter;
import com.example.logisctest.interfaces.MainUi;
import com.example.logisctest.presenters.MainPresenterImpl;

public class MainActivity extends AppCompatActivity implements MainUi {

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainPresenterImpl(this);
    }
}